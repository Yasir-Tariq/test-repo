FROM jenkins/jenkins:lts
WORKDIR  /chef-jenkins
USER root
RUN apt update -y && \
    apt install apt-utils && \
    apt install nano -y && \
    apt install wget -y && \
    DEBIAN_FRONTEND="noninteractive" apt-get -y install tzdata
RUN wget https://packages.chef.io/files/stable/chef-workstation/21.2.278/ubuntu/20.04/chef-workstation_21.2.278-1_amd64.deb
RUN dpkg -i chef-workstation_21.2.278-1_amd64.deb 
RUN chef -v 

RUN knife client list